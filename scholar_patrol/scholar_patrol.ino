/*
 * AUTHOR: Shaun Rust
 * 
 * Copyright (C) 2018
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * OVERVIEW
 * 
 * This project is a controller for a scholar patrol. It operates as a
 * basic single state machine.
 * 
 */


// Pins - Connect each pin to the limiting resister of 80 Ohms.
//        The limit resister value supports 2 LED's in parellel 
const int PEDESTRIAN_GREEN_LED = 8;
const int PEDESTRIAN_RED_LED = 9;
const int TRAFFIC_LIGHT_RED_LED = 10;
const int TRAFFIC_LIGHT_ORANGE_LED = 11;
const int TRAFFIC_LIGHT_GREEN_LED = 12;
const int PEDESTRIAN_BUTTON = 2;

// The loop will poll every 100ms
const int POLL = 100;

// State - Pedestrian light
typedef struct tag_state_pedestrian {
  char *state_name;
  int pin;
  bool flash;
} STATE_PEDESTRIAN;

// State - Traffic light
typedef struct tag_state_traffic {
  char *state_name;
  int pin;
  int duration_seconds;
  struct tag_state_traffic *next;
  bool flash;
  STATE_PEDESTRIAN *pedestrian_state;
} STATE_TRAFFIC;

// Pedestrian light states
STATE_PEDESTRIAN statePedestrianStop = {"PEDESTRIAN - STOP", PEDESTRIAN_RED_LED, false};
STATE_PEDESTRIAN statePedestrianStopWarning = {"PEDESTRIAN - STOP WARNING", PEDESTRIAN_RED_LED, true};
STATE_PEDESTRIAN statePedestrianWalk = {"PEDESTRIAM - WALK", PEDESTRIAN_GREEN_LED, false};

// Traffic light states
STATE_TRAFFIC stateTrafficRedAllStop2 = {"TRAFFIC - RED ALL STOP", TRAFFIC_LIGHT_RED_LED, 2, NULL, false, &statePedestrianStop};
STATE_TRAFFIC stateTrafficRedWarning = {"TRAFFIC - RED WARNING PEDESTRIAN", TRAFFIC_LIGHT_RED_LED, 4, &stateTrafficRedAllStop2, false, &statePedestrianStopWarning};
STATE_TRAFFIC stateTrafficRed = {"TRAFFIC - RED", TRAFFIC_LIGHT_RED_LED, 10, &stateTrafficRedWarning, false, &statePedestrianWalk};
STATE_TRAFFIC stateTrafficRedAllStop = {"TRAFFIC - RED ALL STOP", TRAFFIC_LIGHT_RED_LED, 2, &stateTrafficRed, false, &statePedestrianStop};
STATE_TRAFFIC stateTrafficOrange = {"TRAFFIC - ORANGE", TRAFFIC_LIGHT_ORANGE_LED, 4, &stateTrafficRedAllStop, false,  &statePedestrianStop};
STATE_TRAFFIC stateTrafficGreenWarning = {"TRAFFIC - GREEN WARNING", TRAFFIC_LIGHT_GREEN_LED, 4, &stateTrafficOrange, true, &statePedestrianStop};
STATE_TRAFFIC stateTrafficGreen = {"TRAFFIC - GREEN", TRAFFIC_LIGHT_GREEN_LED, 0, &stateTrafficGreenWarning, false, &statePedestrianStop};

// Initial state of the lights
STATE_TRAFFIC *currentState = &stateTrafficGreen;
int flash_count = 0;
long time_next_state = 0;

void setup() {
  stateTrafficRedAllStop2.next = &stateTrafficGreen;
  
  // Setup serial port for debug
  // Increase the baud rate for improved logging speed.
  Serial.begin(9600);
  
  // Setup pins
  // ?
  pinMode(PEDESTRIAN_RED_LED, OUTPUT);
  pinMode(PEDESTRIAN_GREEN_LED, OUTPUT);
  pinMode(TRAFFIC_LIGHT_RED_LED, OUTPUT);
  pinMode(TRAFFIC_LIGHT_ORANGE_LED, OUTPUT);
  pinMode(TRAFFIC_LIGHT_GREEN_LED, OUTPUT);

  // ?
  pinMode(PEDESTRIAN_BUTTON, INPUT);

  // ?
  Serial.write("Boot: testing lights ...\n");
  digitalWrite(PEDESTRIAN_RED_LED, HIGH);
  digitalWrite(PEDESTRIAN_GREEN_LED, HIGH);
  digitalWrite(TRAFFIC_LIGHT_RED_LED, HIGH);
  digitalWrite(TRAFFIC_LIGHT_ORANGE_LED, HIGH);
  digitalWrite(TRAFFIC_LIGHT_GREEN_LED, HIGH);

  delay(2000);
  
  digitalWrite(PEDESTRIAN_GREEN_LED, LOW);
  digitalWrite(TRAFFIC_LIGHT_RED_LED, LOW);
  digitalWrite(TRAFFIC_LIGHT_ORANGE_LED, LOW);

  Serial.write("Boot: Ok!\n");
}

void loop() {
  // put your main code here, to run repeatedly:
  int buttonState = digitalRead(PEDESTRIAN_BUTTON);
  long currentTime = millis();
  char msg[128];

  if (!currentState->duration_seconds && buttonState == HIGH){
    time_next_state = 1; // This will ensure a none zero state
  }

  if (time_next_state && time_next_state < currentTime) {
    // Reset the flash count
    flash_count = 0;
    
    // Turn the current light off
    digitalWrite(currentState->pin, LOW);
    digitalWrite(currentState->pedestrian_state->pin, LOW);
    // prevState = currentState;
    currentState = currentState->next;
    time_next_state = currentState->duration_seconds > 0 ? currentTime + (currentState->duration_seconds * 1000) : 0;
    
    digitalWrite(currentState->pin, HIGH);
    digitalWrite(currentState->pedestrian_state->pin, HIGH);
    

    sprintf(msg, "currentTime=[%ld], buttonState=[%d], trafficState=[%s], pedestrianState=[%s]\n", currentTime, buttonState, currentState->state_name, currentState->pedestrian_state->state_name);
    Serial.write(msg);
  }

  // Flash traffic check
  if (currentState->flash){
    if ((flash_count++ % 2) == 0){
      digitalWrite(currentState->pin, LOW);
    } else {
      digitalWrite(currentState->pin, HIGH);
    }
  }
  
  // Flash pedestrian check
  if (currentState->pedestrian_state->flash){
    if ((flash_count++ % 2) == 0){
      digitalWrite(currentState->pedestrian_state->pin, LOW);
    } else {
      digitalWrite(currentState->pedestrian_state->pin, HIGH);
    }
  }
  
  delay(POLL);
}
